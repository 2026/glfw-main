#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>
#include <gl\glew.h>
#include <GLFW\glfw3.h>
#include <gl\GL.h>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <thread>



#pragma comment(lib, "glfw3.lib")

/*
class ApplicationWindowManager {
public:
	void ApplicationWindowManager::close_callback(GLFWwindow *window) {
		int index = 0;
		for (int i = 0; i < window_list.size(); i++) {
			if (window_list[i] == window)index = i;
		}
		window_list.erase(window_list.begin() + index);
		glfwDestroyWindow(window);
	};

	void addWindow(int width, int height, const char *title, GLFWmonitor *monitor) {
		GLFWwindow* window_pointer = glfwCreateWindow(width, height, title, monitor, NULL);
		window_list.push_back(window_pointer);
		std::cerr << window_pointer << std::endl;
		const std::function<void(GLFWwindow*)> *s = new std::function<void(GLFWwindow*)>(ApplicationWindowManager::close_callback);

		glfwSetWindowCloseCallback(window_pointer,  s->target);
		glfwShowWindow(window_pointer);
	}



	std::thread* getWindowManagerThread() {
		return this->mThread;
	}


	ApplicationWindowManager::ApplicationWindowManager()
	{
		window_list.reserve(1);
		mThread = new std::thread();
		std::thread t (&ApplicationWindowManager::goOverWindows, this);
		mThread->swap(t);
	}
protected:
	static std::vector<GLFWwindow*> window_list;
	std::thread* mThread;

	void goOverWindows() {
		while (window_list.size() < 1);
		while (window_list.size() > 0) {
			glfwPollEvents();
			for (GLFWwindow* w : window_list) {
				glfwMakeContextCurrent(w);
				glClear(GL_COLOR_BUFFER_BIT);
				glfwSwapBuffers(w);
			}
		}
	};

};
*/

#include "glfw++.cpp"

void sizeCallbackMethod(GLFWwindow* w, int width, int height) {
	std::cerr << width << std::endl;
}

std::function<void(GLFWwindow* w, int width, int height)> sizeCallback = std::function<void(GLFWwindow* w, int width, int height)>(sizeCallbackMethod);

using namespace glfw;

int main() {
	//First create GL context!
	if (!glfwInit())throw new std::exception();
	glfw::Window* w = new glfw::Window(100, 100, "Wrapper API Test", NULL);
	w->show();
	bool needsRefresh = false;
	std::unique_ptr<EventHandler<class WindowEvent>> *resizeHandler = new std::unique_ptr<EventHandler<class WindowEvent>>(new EventHandler<class WindowEvent>(std::function<void(WindowEvent*)>([&](WindowEvent *e) {
		std::cout << e->getHeight() << std::endl;
		needsRefresh = true;
	})));

	std::unique_ptr<EventHandler<class WindowEvent>> *closeHandler = new std::unique_ptr<EventHandler<class WindowEvent>>(new EventHandler<class WindowEvent>(std::function<void(WindowEvent*)>([](WindowEvent *e) {
		std::cout << "Accepting window close request!" << std::endl;
		if (e->getEventSource()->isDestroyed())e->getEventSource()->destroy();

	})));

	w->setOnWindowResized(resizeHandler->get());
	w->setOnWindowCloseRequest(closeHandler->get());
	while (true) {
		if (needsRefresh) {
			w->swapBuffers();
			needsRefresh = false;
		}
		glfw::Window::pollEvents();
		if (w->isDestroyed())goto Cleanup;
		Sleep(50);
	}
	
ApplicationEnd:
	system("pause");
	return 0;
Cleanup:
	std::cerr << "Applying global cleanup!" << std::endl;
	std::cout << "deleting window object!" << std::endl;
	delete w;
	delete resizeHandler->release();
	delete closeHandler->release();
	delete resizeHandler;
	delete closeHandler;
	std::cout << "deleted window object!" << std::endl;
	goto ApplicationEnd;
}
