
#include "glfw++.h"


namespace glfw {



	const enum KeyType : char {
		A, S, D, W, Q, E, R, T, ENTER, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12
	};

	const enum EventType : char {
		KEY_PRESSED,


		MOUSE_ENTERED,
		MOUSE_LEAVED,
		MOUSE_PRESSED,

		WINDOW_RESIZE,
		WINDOW_REFRESH,
		WINDOW_CLOSE,
		WINDOW_FOCUS
	};

	class Event {
	protected:
		EventType type;
		glfw::Window *source;
	public:

		EventType getEventType() {
			return type;
		}

		Window* getEventSource() {
			return source;
		}

		Event::~Event() {
			printf("deleting event!\n");
		}

	};

	class MouseEvent : public glfw::Event {

	public:

		MouseEvent::MouseEvent(glfw::Window* w, EventType mouseEventType) {
			this->source = w;
			this->type = EventType::MOUSE_ENTERED;
		};

		EventType getEventType() {
			return type;
		}

	};

	class KeyEvent : public glfw::Event {
	protected:

		KeyType keyType;

	public:
		KeyEvent::KeyEvent(glfw::Window* w, EventType keyEventType, KeyType pressed_key) {
			this->source = w;
			this->type = keyEventType;
			this->keyType = pressed_key;
		}

		KeyType getPressedKey() {
			return keyType;
		}
	};

	class WindowEvent : public glfw::Event {
		int width, height, focus;
	public:
		WindowEvent::WindowEvent(glfw::Window* w, EventType windowEventType) {
			this->source = w;
			this->type = windowEventType;
		}

		WindowEvent::WindowEvent(glfw::Window* w, int width, int height) {
			this->source = w;
			this->type = EventType::WINDOW_RESIZE;
			this->width = width;
			this->height = height;
		}

		WindowEvent::WindowEvent(glfw::Window* w, int focus) {
			this->source = w;
			this->type = EventType::WINDOW_FOCUS;
			this->focus = focus;
		}

		WindowEvent::~WindowEvent() {
			printf("deleting window event\n");

		}

		int getWidth() {
			if (width != NULL)
				return width;
			return -1;
		}

		int getHeight() {
			if (height != NULL)
				return height;
			return -1;
		}

		int getFocus() {
			if (focus != NULL)
				return focus;
			return -1;
		}

	};

	template <class T>
	class EventHandler {
	public:
		std::function<void(T *e)> handle;
		EventHandler::EventHandler(std::function<void(T *e)> func) {
			handle = func;
		}
	};

	class Window {
	private:
		struct UserPointer {
			Window *w;
			void *userPointer;
		};

		static void windowSizeCallback(GLFWwindow* w, int width, int height) {
			((Window::UserPointer*)glfwGetWindowUserPointer(w))->w->s_callback(w, width, height);
		}

		static void windowCloseCallback(GLFWwindow* w) {
			((Window::UserPointer*)glfwGetWindowUserPointer(w))->w->c_callback(w);
		}

		static void windowFocusCallback(GLFWwindow*w, int arg) {
			((Window::UserPointer*)glfwGetWindowUserPointer(w))->w->focusCallback(w, arg);
		}

		static void windowIconifyCallback(GLFWwindow*w, int arg) {
			((Window::UserPointer*)glfwGetWindowUserPointer(w))->w->iconifyCallback(w, arg);
		}

		static void windowPosCallback(GLFWwindow*w, int x, int y) {
			((Window::UserPointer*)glfwGetWindowUserPointer(w))->w->windowposCallback(w, x, y);
		}

		static void windowRefresh(GLFWwindow*w) {
			((Window::UserPointer*)glfwGetWindowUserPointer(w))->w->refreshCallback(w);
		}

		UserPointer* user_pointer = new UserPointer();
		GLFWwindow* m_window;
		std::vector<const glfw::EventHandler<class glfw::WindowEvent>*> resizeHandlers, closeHandlers, focusHandlers;

		void s_callback(GLFWwindow* w, int width, int height) {
			std::unique_ptr<glfw::WindowEvent> event(new WindowEvent(this, width, height));
			for (const glfw::EventHandler<class glfw::WindowEvent>* handler : resizeHandlers) {
				handler->handle(event.get());
			}
			glfw::WindowEvent* ptr = event.release();
			delete ptr;
		}

		void c_callback(GLFWwindow*w) {
			printf("CLOSING\n");
			std::unique_ptr<glfw::WindowEvent> event(new WindowEvent(this, EventType::WINDOW_CLOSE));
			for (const glfw::EventHandler<class glfw::WindowEvent>*handler : closeHandlers) {
				handler->handle(event.get());
			}
			glfw::WindowEvent* ptr = event.release();
			delete ptr;
		}
		std::function<void(GLFWwindow*, int)> focusCallback = std::function<void(GLFWwindow* w, int)>(
			[&](GLFWwindow* w, int focus) {
			std::unique_ptr<glfw::WindowEvent> event(new WindowEvent(this, focus));
			for (const glfw::EventHandler<class glfw::WindowEvent>*handler : focusHandlers) {
				handler->handle(event.get());
			}
			glfw::WindowEvent* ptr = event.release();
			delete ptr;
		});
		std::function<void(GLFWwindow*, int)> iconifyCallback;
		std::function<void(GLFWwindow* w, int, int)> windowposCallback;
		std::function<void(GLFWwindow* w)> refreshCallback;
		std::function<void(GLFWwindow*, int)> cursorEnterCallback;
		std::function<void(GLFWwindow*, double, double)> cursorPosCallback;
		std::function<void(GLFWwindow*, unsigned int)> charCallback;
		std::function<void(GLFWwindow*, int, const char**)> dropCallback;
		std::function<void(int, const char* errText)> errorCallback;
		/*std::function<GLFWframebuffersizefun> framebufferCallback;
		std::function<GLFWjoystickfun> joistickCallback;
		std::function<GLFWkeyfun> keyCallback;
		std::function<GLFWmonitorfun> monitorCallback;
		std::function<GLFWmousebuttonfun> mousebuttonCallback;
		std::function<GLFWscrollfun> scrollCallback;*/


		void setCallbacks() {
			glfwSetWindowSizeCallback(m_window, Window::windowSizeCallback);
			glfwSetWindowCloseCallback(m_window, Window::windowCloseCallback);
			glfwSetWindowFocusCallback(m_window, Window::windowFocusCallback);
			//glfwSetWindowRefreshCallback(m_window, Window::windowRefresh);
			//glfwSetWindowPosCallback(m_window, Window::windowPosCallback);
		}

	public:
		Window::Window(int width, int height, const char* title, GLFWmonitor* monitor) {
			m_window = glfwCreateWindow(width, height, title, monitor, NULL);
			user_pointer->w = this;
			user_pointer->userPointer = NULL;
			glfwSetWindowUserPointer(m_window, user_pointer);
			setCallbacks();
		}

		void setOnWindowResized(EventHandler<class WindowEvent> *handler) {
			this->resizeHandlers.clear();
			this->resizeHandlers.push_back(handler);
		};

		void setOnWindowCloseRequest(EventHandler<class WindowEvent> *handler) {
			this->closeHandlers.clear();
			this->closeHandlers.push_back(handler);
		}

		void show() {
			glfwShowWindow(m_window);
		}

		void hide() {
			glfwHideWindow(m_window);
		}

		void swapBuffers() {
			glfwSwapBuffers(m_window);
		}

		void destroy() {
			glfwDestroyWindow(m_window);
		}

		static void pollEvents() {
			glfwPollEvents();
		}

		void* getUserPointer() {
			return user_pointer->userPointer;
		}

		void setUserPointer(void* usrpointer) {
			this->user_pointer->userPointer = usrpointer;
		}

		Window::~Window() {

			delete user_pointer;
			resizeHandlers.clear();
			closeHandlers.clear();
		}

		int isDestroyed() {
			return glfwWindowShouldClose(m_window);
		}
	};

};